define papertrail::config (
    String $instance_name = undef,
    Optional[Array] $files = undef,
    Optional[Array] $exclude_files = undef,
    Optional[Array] $exclude_patterns = undef,
    Optional[String] $hostname = undef,
    Integer $destination_port = undef,
    String $destination_host = undef,
    String $destination_protocol = 'tls',
    Optional[Integer] $new_file_check_interval = undef,
    Optional[String] $facility = undef,
    Optional[String] $severity = undef,
){

  file {"/etc/log_files_${instance_name}.yml":
    ensure  => file,
    content => template('papertrail/log_files.yml.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    notify  => Service["remote_syslog_${instance_name}"]
  }
  file {"/etc/init.d/remote_syslog_${instance_name}":
    ensure  => file,
    content => template('papertrail/remote_syslog.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    notify  => Service["remote_syslog_${instance_name}"]
  }
  service {"remote_syslog_${instance_name}":
    ensure    => running,
    enable    => true,
    binary    => '/usr/local/bin/remote_syslog',
    path      => "/etc/init.d/remote_syslog_${instance_name}",
  }
}
