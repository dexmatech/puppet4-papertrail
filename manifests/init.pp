class papertrail (
  String $version = '0.20'
) {

  case $facts['os']['family'] {
    /(RedHat|Amazon)/: {
      $full_pkg_name = "remote_syslog2-${$papertrail::version}-1.x86_64.rpm"
      $pkg_name = 'remote_syslog2'
      $pkg_provider = 'rpm'
    }
    'Debian': {
      $full_pkg_name = "remote-syslog2_${$papertrail::version}_amd64.deb"
      $pkg_name = 'remote-syslog2'
      $pkg_provider = 'dpkg'
    }
    default: { fail('Unsupported OS family') }
  }

  remote_file {"/tmp/${full_pkg_name}":
    source => "http://github.com/papertrail/remote_syslog2/releases/download/v${$papertrail::version}/${full_pkg_name}"
  }

  exec { 'dpkg-deb':
    command => "/usr/bin/dpkg-deb -xv /tmp/${full_pkg_name} /tmp/remote-syslog/",
    require => Remote_File["/tmp/${full_pkg_name}"]
  }

  file { '/usr/local/bin/remote_syslog':
          ensure => present,
          mode   => '0755',
          source => "/tmp/remote-syslog/usr/local/bin/remote_syslog",
  }
}
